# Tier 3 Support Engineer Pairing Exercise

The purpose of this exercise is to evaluate one's ability to run basic SQL queries to find and possibly correct data.  

The data is derived from the [Northwind sample database](https://docs.yugabyte.com/latest/sample-data/northwind/) from [yugabyteDB](https://www.yugabyte.com/)

## Initial Setup

1. Open [https://sqliteonline.com/](https://sqliteonline.com/) in a browser.  The exercise will be performed here.
1. Select "PostgreSQL" on the left hand side.
1. Remove the existing "demo" table.
1. Copy and paste the contents of the file `ddl.sql` from this repo into the command/query area and run.
1. Copy and paste the contents of the file `data.sql` from this repo into the command/query area and run
1. The database is now ready to solve the following scenario

## Scenario

- Francisco Chang, the Marketing Manager from Centro comercial Moctezuma has called customer support stating that they believe they have been overcharged for one of the products in their order.
- Francisco states that they were promised a price of $2.00 less than the listed unit price on a product called "Sir Rodney's Scones" if they bought a quantity of 10 or more.  Francisco believes they were instead charged extra resulting in a much higher bill than expected.
- Francisco has threatened to find a new supplier if this is not corrected immediately and a new invoice produced.

## Task 

1. Find and correct (if possible) the errant data.  
1. Report the finding and correction to Customer Support
    1. (For interview purposes, discuss the steps to be taken and how those would best be communicated)
1. Discuss, document, and report the bug (if one exists)
    1. (Discuss how to report the bug, what information is relevant to the software developer, etc)

## ERD

![ERD](https://gitlab.com/mduffield7/t3-support-pairing/-/raw/main/northwind-er-diagram.png)
